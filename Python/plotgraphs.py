#!/usr/bin/env python
import os
import pandas as pd
import matplotlib.pyplot as plt
import re
from matplotlib.pyplot import figure

### Set your path to the folder containing the .csv files
PATH = './' # Use your path

### Fetch all files in path
fileNames = os.listdir(PATH)

### Filter file name list for files ending with .csv
fileNames = [file for file in fileNames if '.csv' in file]
pd.options.display.max_rows = 4000
### Loop over all files
fig, ax = plt.subplots()
ax.set_yscale('log')
ax.set_ylabel('time')
fig.set_size_inches(18.5, 10.5)
for file in fileNames:

    ### Read .csv file and append to list
    #df = pd.read_csv(PATH + file, sep=";")
    
    ### Read .csv without header
    colnames=['name', 'depth', 'time'] 
    df = pd.read_csv(PATH + file, sep=',', names=colnames, header = None)
        
    df['name'] = df['name'].map(lambda x: re.findall(r'(\d+(?:\.\d+)?)', x)[0]).astype(int)
    df = df.sort_values(by='name')
    ### Create line for every file
    df.plot.line(x ='name', y='time', ax=ax, label=str(file))

plt.savefig('vertex.png', bbox_inches='tight')
plt.show()

### Loop over all files
fig, ax = plt.subplots()
ax.set_yscale('log')
ax.set_ylabel('time')
fig.set_size_inches(18.5, 10.5)
for file in fileNames:

    ### Read .csv file and append to list
    #df = pd.read_csv(PATH + file, sep=";")
    
    ### Read .csv without header
    colnames=['name', 'depth', 'time'] 
    df = pd.read_csv(PATH + file, sep=',', names=colnames, header = None)
        
    df['name'] = df['name'].map(lambda x: re.findall(r'(\d+(?:\.\d+)?)', x)[0]).astype(int)
    df_na_sorted = df.sort_values(by='depth', na_position='first')
    df_na_sorted.plot.line(x ='depth', y='time', ax = ax, label=str(file))
    
### Generate the plot
plt.savefig('depth.png', bbox_inches='tight')
plt.show()

### Loop over all files
fig, ax = plt.subplots()
fig.set_size_inches(18.5, 10.5)
for file in fileNames:

    ### Read .csv file and append to list
    #df = pd.read_csv(PATH + file, sep=";")
    
    ### Read .csv without header
    colnames=['name', 'depth', 'time'] 
    df = pd.read_csv(PATH + file, sep=',', names=colnames, header = None)
        
    df['name'] = df['name'].map(lambda x: re.findall(r'(\d+(?:\.\d+)?)', x)[0]).astype(int)
    df = df.sort_values(by='name')
    ### Create line for every file
    df.plot.scatter(x ='name', y='depth', ax=ax, label=str(file))
    
### Generate the plot
plt.show()
