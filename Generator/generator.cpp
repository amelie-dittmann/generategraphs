// Generator.cpp : Diese Datei enth�lt die Funktion "main". Hier beginnt und endet die Ausf�hrung des Programms.
//

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <numeric>
#include <random>


void makeOutput(int edges[][2], int vertex_num, int edges_num)
{
	std::cout << "p tdp " << vertex_num << " " << edges_num;
	for (int i = 0; i < edges_num; i++)
	{
		std::cout << "\n";
		if (rand() % 2)
			std::cout << edges[i][0] << " " << edges[i][1];
		else
			std::cout << edges[i][1] << " " << edges[i][0];

	}
	std::cout << "\n";
	std::cout << "c td " << edges[edges_num][0];
}

struct node {
	int data;
	std::vector<struct node*> parents; //Reference to parent
	int depth;
	std::vector<struct node*> children;
};

//allocates new node 
struct node* newNode(int data, std::vector<struct node*> parents) {
	// declare and allocate new node  
	struct node* node = new struct node();

	node->data = data;		// Assign data to this node
	node->parents = parents;	//Assign parent node to this node

	node->depth = parents.size();
	/*
	if (parent == null)
		node->depth = 0;
	else
		node->depth = parents->depth + 1;
		*/
	return(node);
}


// builds a tree recursive: call on root node with null, treearray, root
struct node* buildTree(std::vector<struct node*> node, int treeArray[], int vertex, int vertex_num)
{
	std::vector<struct node*> children;
	struct node* self = newNode(vertex + 1, node);
	std::vector<struct node*> parents(node);
	parents.insert(parents.end(), self);
	for (int i = 0; i < vertex_num; i++)
	{
		if (treeArray[vertex + i * vertex_num] == 1) {
			treeArray[vertex + i * vertex_num] = 0;
			treeArray[i + vertex * vertex_num] = 0;
			children.insert(children.end(), buildTree(parents, treeArray, i, vertex_num));
		}
	}
	self->children = children;
	return self;
}

int buildAdjazenzmatrix(struct node* node, int edges[][2], int counter)
{
	for (auto vertex = node->children.begin(); vertex != node->children.end(); vertex++)
	{
		for (auto parent = (*vertex)->parents.begin(); parent != (*vertex)->parents.end(); parent++)
		{
			if (edges[counter][0] < (*vertex)->depth)
				edges[counter + 1][0] = (*vertex)->depth;
			else
				edges[counter + 1][0] = edges[counter][0];
			edges[counter][0] = (*parent)->data;
			edges[counter][1] = (*vertex)->data;
			counter++;
			counter = buildAdjazenzmatrix((*vertex), edges, counter);
		}
	}
	return counter;
}



// generates array like Prufer code for random tree
std::vector<int> generatePruferCode(int vertex)
{
	int length = vertex - 2;
	std::vector<int> arr(length);

	// Loop to Generate Random Array
	for (int i = 0; i < length; i++)
	{
		arr[i] = (rand() % (vertex)) + 1;
		std::cout << "\n";
		std::cout << arr[i];
	}
	std::cout << "\n";
	return arr;
}

void getGraphFromTreeCode(std::vector<int> arr, int vertex_num)
{
	int size = vertex_num;
	std::vector<int> vertex_set(size);
	int counter = 0;
	bool seenVertex = false;
	int rootNode = 0; // safe root node
	int* treeArray;
	treeArray = (int*)malloc(vertex_num * vertex_num * sizeof(int));
	int index = 0;
	// Initialize the array of vertices
	for (int i = 0; i < vertex_num; i++)
	{
		vertex_set[i] = 0;
		for (int j = 0; j < vertex_num; j++) {
			index = i + j * vertex_num;
			treeArray[index] = 0;
		}

	}


	// Number of occurrences of vertex in code
	for (int i = 0; i < vertex_num - 2; i++)
		vertex_set[arr[i] - 1] += 1;

	for (int value : arr) {
		for (int i = 0; i < vertex_num; i++) {

			// select smallest vertex which is not
			// in vertex set
			if (vertex_set[i] == 0)
			{
				// remove
				vertex_set[i] = -1;
				vertex_set[value - 1]--;

				if (!seenVertex) {
					rootNode = value - 1;
					seenVertex = true;
				}

				treeArray[i + (value - 1) * vertex_num] = 1;
				treeArray[(value - 1) + i * vertex_num] = 1;

				std::cout << (value - 1) << "; " << i << "\n";
				for (int k = 0; k < vertex_num; k++) {
					for (int j = 0; j < vertex_num; j++) {
						std::cout << treeArray[k + j * vertex_num] << " ";
					}
					std::cout << "\n";
				}
				break;
			}
		}
	}

	int j = 0;
	int firstElement = -1;
	// For the last two element
	for (int i = 0; i < vertex_num; i++) {
		if (vertex_set[i] == 0 && firstElement == -1)
			firstElement = i;
		else if (vertex_set[i] == 0) {
			if (!seenVertex) {
				rootNode = i;
				seenVertex = true;
			}

			treeArray[i + firstElement * vertex_num] = 1;
			treeArray[firstElement + i * vertex_num] = 1;

			break;
		}
	}


	std::vector<struct node*> emptyVector;
	struct node* root = buildTree(emptyVector, treeArray, rootNode, vertex_num);
	auto edges{ new int[size * size][2] };

	int edges_num = buildAdjazenzmatrix(root, edges, 0);
	makeOutput(edges, vertex_num, edges_num);

	free(treeArray);
}

std::vector<int> permutateNumberString(int vertex)
{
	std::vector<int> v(vertex);
	std::iota(v.begin(), v.end(), 1);

	std::random_device rd;
	std::mt19937 g(rd());

	std::shuffle(v.begin(), v.end(), g);

	//std::copy(v.begin(), v.end(), std::ostream_iterator<int>(std::cout, " "));
	return v;
}



void getGraphBipartite(int n, int m, std::vector<int> permutatedString)
{
	int size = m * (n + m);
	auto edges{ new int[size + 1][2] };
	int iterator = 0;
	for (int i = 0; i < n; i++)
	{
		for (int j = n; j < n + m; j++)
		{
			edges[iterator][0] = permutatedString[i];
			edges[iterator][1] = permutatedString[j];
			iterator++;
		}
	}
	edges[iterator][0] = std::min(n, m);
	makeOutput(edges, n + m, iterator);
}


int main()
{
	std::vector<int> v = permutateNumberString(5);
	getGraphBipartite(2, 3, v);

	std::vector<int> w = generatePruferCode(4);
	getGraphFromTreeCode(w, 4);
}
